Source: docker-runc
Section: devel
Priority: optional
Maintainer: Tim Potter <tpot@debian.org>
Uploaders: Tianon Gravi <tianon@debian.org>
Build-Depends: debhelper (>= 9),
               dh-exec,
               dh-golang,
               go-md2man,
               golang-any,
               golang-dbus-dev,
               golang-github-codegangsta-cli-dev (>= 0.0~git20151221~),
               golang-github-coreos-go-systemd-dev,
               golang-github-docker-go-units-dev,
               golang-github-mrunalp-fileutils-dev,
               golang-github-seccomp-libseccomp-golang-dev,
               golang-github-vishvananda-netlink-dev,
               golang-github-xeipuuv-gojsonschema-dev,
               golang-gocapability-dev | golang-github-syndtr-gocapability-dev,
               golang-goprotobuf-dev,
               golang-github-sirupsen-logrus-dev (>= 1.0.2~),
               libapparmor-dev,
               protobuf-compiler,
               rename
Standards-Version: 4.1.4
Homepage: https://github.com/opencontainers/runc
Vcs-Browser: https://salsa.debian.org/docker-team/docker-runc
Vcs-Git: https://salsa.debian.org/docker-team/docker-runc.git
XS-Go-Import-Path: github.com/opencontainers/runc

Package: docker-runc
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Breaks: docker.io (<< 1.12)
Built-Using: ${misc:Built-Using}
Description: Open Container Project - runtime (Docker's version)
 "runc" is a command line client for running applications packaged according
 to the Open Container Format (OCF) and is a compliant implementation of
 the Open Container Project specification.
 .
 This package contains the version to be used with docker.io packages and
 provides "runc" as the "docker-runc" executable binary.

Package: golang-github-opencontainers-docker-runc-dev
Architecture: all
Depends: golang-dbus-dev,
         golang-github-codegangsta-cli-dev (>= 0.0~git20151221~),
         golang-github-coreos-go-systemd-dev,
         golang-github-docker-go-units-dev,
         golang-github-seccomp-libseccomp-golang-dev,
         golang-github-vishvananda-netlink-dev,
         golang-gocapability-dev | golang-github-syndtr-gocapability-dev,
         golang-goprotobuf-dev,
         golang-github-sirupsen-logrus-dev (>= 1.0.2~),
         ${misc:Depends}
Provides: golang-github-opencontainers-runc-dev
Conflicts: golang-github-opencontainers-runc-dev
Description: Open Container Project - development files (Docker's version)
 "runc" is a command line client for running applications packaged according
 to the Open Container Format (OCF) and is a compliant implementation of
 the Open Container Project specification.
 .
 This package provides development files formerly known as
 "github.com/docker/libcontainer".
 .
 This package contains the version to be used with docker.io packages.
